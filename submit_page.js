// Initialize Firebase
'use strict';
//fields of a new post
var signInButton = document.getElementById('sign-in-button');
var splashPage = document.getElementById('page-splash');
var signOutButton = document.getElementById('sign-out-button');
var newNoticeMenuButton = document.getElementById('menu-add-post');
var addPostSection = document.getElementById('section-add-post');


//Refernce to default section
var defaultSection = document.getElementById('default-section');



var collegeDepartmentSection =document.getElementById('section-college-post');

//refernce to tempalte image
var TemplateImageSection =document.getElementById('section-template-image');

//reference to image button
var upladImageButton=document.getElementById('button-add-images');
var chooseImageFiles= document.getElementById('button-add-images').files;


//MypostButton
var mypostMenuButton=document.getElementById('menu-my-posts');
var mypostSection=document.getElementById('section-my-post');


//reference to files button
var upladFileButton=document.getElementById('button-add-files');
var chooseFiles= document.getElementById('button-add-files').files;

var writePathsCollege=['College','College/Comp','College/ENTC','College/IT','College/FE']
var writePaths=['ArtCircle','PISB','PASC'];
var vDescription=document.getElementById('description');
var vImage=document.getElementById('image');


var vOther=document.getElementById('other');
var vTitle=document.getElementById('title');
var vUrl=document.getElementById('url');
var vForm = document.getElementById('form-submit-notice');
var listeningFirebaseRefs = [];

//User variables

var userWritePathPermission=''
var currentUID=''
//================================
//23/2/2017
//Akshay Patel

/**
 * Cleanups the UI and removes all Firebase listeners.
 */
function cleanupUi() {
  // Remove all previously displayed posts.
  //topUserPostsSection.getElementsByClassName('posts-container')[0].innerHTML = '';
  //recentPostsSection.getElementsByClassName('posts-container')[0].innerHTML = '';
 mypostSection.getElementsByClassName('posts-container')[0].innerHTML = '';

  // Stop all currently listening Firebase listeners.
  listeningFirebaseRefs.forEach(function(ref) {
    ref.off();
  });
  listeningFirebaseRefs = [];
}//end clean up ui



/**
 * Writes the user's data to the database.
 */
// [START basic_write]
function writeUserData(currentUID, name, email, imageUrl) {
  firebase.database().ref('users/' + currentUID).set({
    username: name,
    email: email,
    profile_picture : imageUrl
  });
}
// [END basic_write]


//[Start showSection]
function showSection(sectionElement, buttonElement) {
  addPostPostsSection.style.display = 'none';
  if (sectionElement) {
    sectionElement.style.display = 'block';
  }
  if (buttonElement) {
    buttonElement.classList.add('is-active');
  }
}//[END showSection]



/**
 * The ID of the currently signed-in User. We keep track of this to detect Auth state change events that are just
 * programmatic token refresh but not a User status change.
 */
var currentUID;

/**
 * Triggers every time there is a change in the Firebase auth state (i.e. user signed-in or user signed out).
 */
function onAuthStateChanged(user) {
  // We ignore token refresh events.
  if (user && currentUID === user.uid) {
    return;
  }

  cleanupUi();
  if (user) {
    currentUID = user.uid;
    splashPage.style.display = 'none';
    writeUserData(user.uid, user.displayName, user.email, user.photoURL);
    startDatabaseQueries();
  } else {
    // Set currentUID to null.
    currentUID = null;
    // Display the splash page where you can sign-in.
    splashPage.style.display = '';
  }
}//[END onAuthStateChanged]

function startDatabaseQueries() {
  // [START my_top_posts_query]
checkUserPermission(function(writePath1)
{

	if(!(writePaths.indexOf(writePath1)!==-1||writePathsCollege.indexOf(writePath1)!==-1))
	{//[No Permission]
	alert("YOU DONT HAVE ANY PERMISSION TO ACCESS THIS PAGE")
	document.getElementById('permission-status').innerHTML='PERMISSION NOT GRANTED'
	}
	else
	{
	document.getElementById('permission-status').innerHTML='PERMISSION GRANTED'
	}
})//[END check user's permission Initially]

}


// Bindings on load.
window.addEventListener('load', function() {
 
 //signin to login
  signInButton.addEventListener('click', function() {
	console.log("ONCLICK login Button");
    var provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(provider);
  });

 // Bind Sign out button.
  signOutButton.addEventListener('click', function() {
    firebase.auth().signOut();
  });

// Listen for auth state changes
  firebase.auth().onAuthStateChanged(onAuthStateChanged);


//Add new post
newNoticeMenuButton.onclick = function() {
//Don't show the default section
 defaultSection.style.display="none";

//Don't Show the section to View Notices
mypostSection.style.display="none";

//console.log("newNoticeMenuButton  Click add new post");

//Show the section to add Notices
addPostSection.style.display="block";

//Show the section to View Template Images
TemplateImageSection.style.display;
TemplateImageSection.style.display="block";
if(userWritePathPermission==='College')
{
collegeDepartmentSection.style.display="block";
addCommmonTemplateImages();
}
else if(writePathsCollege.indexOf(userWritePathPermission)!==-1)
{
//need to fix this yet
var path=userWritePathPermission.split("/")[1]
if(path==='Comp')
addTemplateImages('template-image-loader',arrComp,path,"img");
else if(path==='IT')
addTemplateImages('template-image-loader',arrIT,path,"img");
else if(path==='FE')
addTemplateImages('template-image-loader',arrFE,path,"img");
else if(path==='EnTc')
addTemplateImages('template-image-loader',arrEnTc,path,"img");

}
else if(userWritePathPermission==='PASC')
{
addTemplateImages('template-image-loader',arrPISB,userWritePathPermission,"img");
}
else if(userWritePathPermission==='PISB')
{
addTemplateImages('template-image-loader',arrPASC,userWritePathPermission,"img");
}
  };//[END LISTENER FOR newNoticeMenuButton]


   vForm.onsubmit = function(e) {
   e.preventDefault();
   writeNewPost(userWritePathPermission,currentUID);

  };

//[START MyPostSection]
mypostMenuButton.onclick=function()
{
 cleanupUi();
defaultSection.style.display="none";
console.log("mypostMenuButton Click myPost botton");
addPostSection.style.display="none";
collegeDepartmentSection.style.display="none";
TemplateImageSection.style.display="none";
mypostSection.style.display="block";

var userPostsRef = firebase.database().ref('/'+userWritePathPermission);
  fetchPosts(userPostsRef, mypostSection);
  //Saves message on form submit.

}//[END MyPostSection]

}, false);
//[END window.onload]

function checkUserPermission(callback)
{
//Setting global variable user id
//userid=firebase.auth().currentUser.uid;
console.log("Checking permissions for "+ currentUID);

var permission = firebase.database().ref('/Permissions/' + currentUID).once('value').then(function(snapshot) {
userWritePathPermission = snapshot.val();

console.log('Permission__ : '+userWritePathPermission )
callback(userWritePathPermission);
});
}//[END checkUserPermission]



