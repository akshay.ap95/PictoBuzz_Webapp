console.log("File 2")

//[Start Fetch Notices]
  var fetchPosts = function(postsRef, sectionElement) {
    postsRef.on('child_added', function(data) {
      var author = data.val().uid || 'Anonymous';
      var containerElement = sectionElement.getElementsByClassName('posts-container')[0];
      containerElement.insertBefore(
          createPostElement(data.key, data.val().Title,data.val().Description,data.val().NoticeNumber),
          containerElement.firstChild);
    });
    postsRef.on('child_changed', function(data) {	
		var containerElement = sectionElement.getElementsByClassName('posts-container')[0];
		var postElement = containerElement.getElementsByClassName('post-' + data.key)[0];
		postElement.getElementsByClassName('mdl-card__title-text')[0].innerText = data.val().Title;
		postElement.getElementsByClassName('username')[0].innerText = data.val().NoticeNumber;
		postElement.getElementsByClassName('text')[0].innerText =data.val().Description;
		
    });
    postsRef.on('child_removed', function(data) {
		var containerElement = sectionElement.getElementsByClassName('posts-container')[0];
		var post = containerElement.getElementsByClassName('post-' + data.key)[0];
	    post.parentElement.removeChild(post);
    });
  };
//[END fetch Notice]

//[Delete Post Transaction]
function Deletepost(postRef, postId) {
  postRef.transaction(function(post) {
    if (post) {
     post.NoticeNumber= -post.NoticeNumber;
    }
    return post;
  });

alert("Notice has been Deleted");
}
//[End delete Post Transaction]

//[Delete click listeser for Delete Post Button]
var onDeleteClicked = function() {
 	var PostRef = firebase.database().ref(userWritePathPermission+'/'+postId);
	Deletepost(PostRef,postId);
 };



//[Start Create Post Element]
function createPostElement(postId, title, text, noticeNumber) {
  var uid = firebase.auth().currentUser.uid;
  var html =
      '<div class="post post-' + postId + ' mdl-cell mdl-cell--12-col ' +
                  'mdl-cell--6-col-tablet mdl-cell--4-col-desktop mdl-grid mdl-grid--no-spacing">' +
        '<div class="mdl-card mdl-shadow--2dp">' +
          '<div class="mdl-card__title mdl-color--light-blue-600 mdl-color-text--white">' +
            '<h4 class="mdl-card__title-text"></h4>' +
	
          '</div>' +
          '<div class="header">' +
	'<div id="delete_notice_button" class="mdl-button material-icons">Delete Notice</div>' +
            '<div>' +
              '<div class="username mdl-color-text--black"></div>' +
            '</div>' +
          '</div>' +
          '<div class="text"></div>' +
          '<form class="add-comment" action="#">' +
            '<div class="mdl-textfield mdl-js-textfield">' +              
            '</div>' +
          '</form>' +
        '</div>' +
      '</div>';

  // Create the DOM element from the HTML.
  var div = document.createElement('div');
  div.innerHTML = html;
  var postElement = div.firstChild;
  if (componentHandler) {
    componentHandler.upgradeElements(postElement.getElementsByClassName('mdl-textfield')[0]);
  }
  // Set values.
  postElement.getElementsByClassName('text')[0].innerText ='Description :\n' +text;
  postElement.getElementsByClassName('mdl-card__title-text')[0].innerText = title;
  postElement.getElementsByClassName('username')[0].innerText ='Notice Number : '+noticeNumber;
  var delete_button=postElement.getElementsByClassName('mdl-button material-icons')[0];
 
  var onDeleteClicked = function() {
 	var PostRef = firebase.database().ref(userWritePathPermission+'/'+postId);
	Deletepost(PostRef,postId);
 };
   delete_button.onclick = onDeleteClicked;

  return postElement;
}
//[END Create Post Element]
