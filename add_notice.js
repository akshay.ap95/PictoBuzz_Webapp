console.log("Add Notice section")

var NoticeTemplateImage =document.getElementById('notice-image-template');


//[START write_fan_out]
function writeNewPost(UserWritePath,userId) {
//Check if the User has permission to Add the NOTICE
if(writePathsCollege.indexOf(UserWritePath)!==-1||writePaths.indexOf(UserWritePath)!==-1)
	{
	//Add date
	var d = new Date();
	var utc = d.getTime() + (d.getTimezoneOffset() * 60000);
	var nd = new Date(utc + (3600000*+5.5));
	var ist =  nd.toLocaleString();
	//Add UID
	var postuserID=userId
 	// A post entry.
 	 var postData = {
	DateofUpload:ist,
	Description: vDescription.value,
  	Title:vTitle.value,
   	Other:vOther.value,
   	Url: vUrl.value,
	uid:postuserID,
	Image: TemplateImageUrl,
	};

	TemplateImageUrl=""
	//Permitted to post
	if(writePathsCollege.indexOf(UserWritePath)!==-1)
	{//[Start General college]

		var specificWrite=UserWritePath.split("/");
		if(specificWrite.length==1)
		{//ReaD VALUE FROM radio button to set the path for notice fill this
		console.log("Pushing College data")
		var noticeNumber=-100
		resolveCollegeUpload(function(innerPath,notNum){
		postData['NoticeNumber']=notNum;
		var newPostKey = firebase.database().ref().child('College').child(innerPath).push().key;
		console.log('Inner PAth in college'+ innerPath)
		console.log('Notice number in college'+ notNum)
		writeFilesAndImages(UserWritePath+'/'+innerPath+'/',newPostKey,postData);
		})//[END resolveCollegeUpload]
		}//[End  General college]
		else
		{//[Start Else]
		console.log('Specific to departMent')
		var hashMap={};
		hashMap['College/Comp']='NumberComp';
		hashMap['College/IT']='NumberIT';
		hashMap['College/EnTc']='NumberEnTc';
		hashMap['College/FE']='NumberFE';
		firebase.database().ref().child('College').child(hashMap[UserWritePath]).once('value').then(function(snapshot) {
		var noticeNumber2 = snapshot.val();
		console.log("Specidifc College Notice Number is: ===="+noticeNumber2);
		var updatesnot = {};
		updatesnot['/College/'+hashMap[UserWritePath]]=noticeNumber2+1;
		firebase.database().ref().update(updatesnot);
		var newPostKey = firebase.database().ref().child('College').child(specificWrite[1]).push().key;
		postData['NoticeNumber']=noticeNumber2+1;
		writeFilesAndImages(UserWritePath+'/',newPostKey,postData);
	});
	}//[END ELSE]
	}//[END if college]
	else 
	{
	console.log('Writeing through non College Notice')
	var newPostKey = firebase.database().ref().child(UserWritePath).push().key;
	postData['NoticeNumber']=-221;
	writeFilesAndImages(UserWritePath+'/',newPostKey,postData);
	}
	}//[END IF CORRECTWRITEPATH]
else 
{
	//NOT PERMITTED to POST
	alert('You Don\'t have permission to add the Notice');
}

}//[END WRITE FAN OUT]


function writeFilesAndImages(writePath1,newPostKey,postData){
//[START WRITE FILES AND IMAGES]
UploadImages(writePath1+newPostKey,function(ImageUrls)	{
	//[ImageUrls]	contain url of all the images relevent to the notice
	postData['Image1']=ImageUrls;
		//[Start UPlaoding FIles]
		UploadFiles(writePath1+newPostKey,function(FileUrls){
		postData['FileUrl']=FileUrls;
		var updates = {};
		//upladImageButton.value="";
		//upladFileButton.value="";

  		PostFinalNotice(writePath1,newPostKey,postData);
		//upladImageButton.value="";
		//upladFileButton.value=""


		})//[Stop UPLOADING FILES]
	
	})//[Stop Uploading Files]
		
//callback(postData);

}//[END WRITE FILES AND IMAGES]

//[START POSTFinalNOtice]
function PostFinalNotice(writePath1,newPostKey,postData)
{
	console.log("Ultimate Posting is with "+ writePath1);
	console.log(JSON.stringify(postData));
	var updates = {};
	updates[writePath1 + newPostKey] = postData;
	//upladImageButton.value="";
	//upladFileButton.value="";

	return firebase.database().ref().update(updates);
}//[End POSTFinalNOtice]





function UploadImages(path,callback)
{	var ImageUrls=[]
	console.log('Uplaoding File to : '+path +'/images')
	var numFiles=chooseImageFiles.length;
	console.log('Number of files is :' + numFiles);
	if(numFiles===0)
	{
		console.log('NO FILES TO UPLOAD');
		callback(ImageUrls)
	}
	var uploaded=0;
	for (var nFileId = 0; nFileId < numFiles; nFileId++) {
	
   	 console.log("File Number "+nFileId+"  "+chooseImageFiles[nFileId].name);
		UploadFile(chooseImageFiles[nFileId],path+'/images',function (downloadUrl){
		ImageUrls.push(downloadUrl)
		uploaded++;
		if(uploaded===numFiles)
		{
		console.log('Uploading OVer' + numFiles);
		callback(ImageUrls)
		}
	
		
		})
	
  	}
	
//callback('Done Uploading');
}//[END UPLOAD IMAGES]

function UploadFile(file,path,callback)
{

//[START UPLOAD FILE]
var storageRef = firebase.storage().ref();
	//var file=chooseImageFiles[0];
	var uploadTask = storageRef.child(path+'/'+file.name).put(file);
	// Regster three observers:
	// 1. 'state_changed' observer, called any time the state changes
	// 2. Error observer, called on failure
	// 3. Completion observer, called on successful completion
	uploadTask.on('state_changed', function(snapshot){
  	// Observe state change events such as progress, pause, and resume
  	// Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
  	var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
  	console.log('Upload is ' + progress + '% done');
  	switch (snapshot.state) {
    	case firebase.storage.TaskState.PAUSED: // or 'paused'
      	console.log('Upload is paused');
      	break;
    	case firebase.storage.TaskState.RUNNING: // or 'running'
      	console.log('Upload is running');
      	break;
  	}

	}, function(error) {
	  // Handle unsuccessful uploads
	}, function() {
 	 // Handle successful uploads on complete
 	 // For instance, get the download URL: https://firebasestorage.googleapis.com/...
	  var downloadURL = uploadTask.snapshot.downloadURL;
	console.log(downloadURL)
	callback(downloadURL)
	});
//[END Upload File]
}



function UploadFiles(path,callback)
{	var FileUrls=[]
	console.log('Uplaoding File to : '+path +'/files')
	var numFiles=chooseFiles.length;
	console.log('Number of files is :' + numFiles);
if(numFiles===0)
	{
console.log('NO FILES TO UPLOAD');
		callback( FileUrls)
}
	var uploaded=0;
	for (var nFileId = 0; nFileId < numFiles; nFileId++) {
	
   	 console.log("File Number "+nFileId+"  "+chooseFiles[nFileId].name);
		UploadFile(chooseFiles[nFileId],path+'/files',function (downloadUrl){
		FileUrls.push(downloadUrl)
		uploaded++;
		if(uploaded===numFiles)
		{
		console.log('Uploading OVer' + numFiles);
		callback(FileUrls)
		}
	
		
		})
	
  	}
//callback('Done Uploading');
}//[END UPLOAD IMAGES]




function resolveCollegeUpload(callback)
{//[STRAT resolveCollegeUpload]
	var noticeNumber=-1;
	var collegePath=''
	var toUpdate='';
//[ START Check Which Radio Button is True]	
	if (document.getElementById('radioButtonCommon').checked) {
	firebase.database().ref().child('College').child('NumberCommon').once('value').then(function(snapshot) {
	noticeNumber = snapshot.val();
	collegePath='Common';

	var updatesnot = {};
	updatesnot['/College/NumberCommon']=noticeNumber+1;
	firebase.database().ref().update(updatesnot);
	
	callback(collegePath,noticeNumber+1);
	});
	}
	else if (document.getElementById('radioButtonFe').checked) {
	firebase.database().ref().child('College').child('NumberFE').once('value').then(function(snapshot) {
	noticeNumber = snapshot.val();
	collegePath='FE';

	var updatesnot = {};
	updatesnot['/College/NumberFE']=noticeNumber+1;
	firebase.database().ref().update(updatesnot);

	
	callback(collegePath,noticeNumber+1);
	});
	}
	else if (document.getElementById('radioButtonComp').checked) {
	firebase.database().ref().child('College').child('NumberComp').once('value').then(function(snapshot) {
	noticeNumber = snapshot.val();
	collegePath='Comp';
	toUpdate='NumberComp';	

	var updatesnot = {};
	updatesnot['/College/NumberComp']=noticeNumber+1;
	firebase.database().ref().update(updatesnot);
	

	callback(collegePath,noticeNumber+1);
	});
	}
	else if (document.getElementById('radioButtonEnTC').checked) {
	firebase.database().ref().child('College').child('NumberEnTc').once('value').then(function(snapshot) {
	noticeNumber = snapshot.val();
	collegePath='EnTc';

	var updatesnot = {};
	updatesnot['/College/NumberEnTc']=noticeNumber+1;
	firebase.database().ref().update(updatesnot);
	callback(collegePath,noticeNumber+1);

	});
	}
	else if (document.getElementById('radioButtonIT').checked) {
	firebase.database().ref().child('College').child('NumberIT').once('value').then(function(snapshot) {
	noticeNumber = snapshot.val();
	collegePath='IT';
	toUpdate='NumberIT';
	 var updatesnot = {};
	updatesnot['/College/NumberIT']=noticeNumber+1;
	firebase.database().ref().update(updatesnot);
	callback(collegePath,noticeNumber+1);
	});
}
//[ END Check Which Radio Button is True]	
}//[END resolveCollegeUpload]

